package Pages;

import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import Tests.BaseTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {	

	public WebDriver driver;
	public ExtentReports report;
	public ExtentTest test;
	
	public LoginPage(WebDriver driver, ExtentReports report, ExtentTest test) {
		this.driver = driver;
		this.report = report;
		this.test = test;
	}	
	
	public WebElement element()
	{
		WebElement signInbutton = null;
		try {
			signInbutton = driver.findElement(By.xpath("//a[@id='nav-link-accountList']"));
		}
		catch(Exception e)
		{
			System.out.println("Some error occurred:");
			System.out.println(e.toString());
		}
		return signInbutton;
	}

	//public WebElement signInbutton = driver.findElement(By.xpath("//a[@id='nav-link-accountList']"));
	//public static By signIn = By.xpath("//a[@id='nav-link-accountList']");
}