package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base 
{
	public ExtentReports report;
 	public ExtentTest test;	
	public ChromeOptions chromeOptions;
	public WebDriver driver;
	
	public Base()
	{
		ExtentReports report = new ExtentReports(".\\reports\\"+"ExtentReportResults.html");;
		ChromeOptions chromeOptions = new ChromeOptions();
		WebDriverManager.chromedriver().setup(); 
		WebDriver driver = new ChromeDriver(chromeOptions);		
	}
	
	public ExtentReports getReport()
	{
		return report;
	}
	
	public WebDriver getDriver()
	{
		return driver;
	}
	
	public ExtentTest getTest()
	{
		return test;
	}
	
	public void startTest1(String testName, String testDescription)
	{
		test = report.startTest(testName, testDescription);
	}
	
	public void addStep1(String status, String stepName, String stepDescription)
	{
		if(status.equalsIgnoreCase("PASS"))
		{
			test.log(LogStatus.PASS, stepName, stepDescription);			
		}
		if(status.equalsIgnoreCase("FAIL"))
		{
			test.log(LogStatus.FAIL, stepName, stepDescription);
		}
	}
}