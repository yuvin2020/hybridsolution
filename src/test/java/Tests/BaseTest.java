package Tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.testng.ITestResult;
import org.testng.annotations.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Pages.Base;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {

	public ExtentReports report;
 	public ExtentTest test;	
	public ChromeOptions chromeOptions;
	public WebDriver driver;
	ITestResult result;
	
	//Base base = new Base();
	
	/*
	 * public BaseTest() { report = new
	 * ExtentReports(".\\reports\\"+"ExtentReportResults.html"); chromeOptions = new
	 * ChromeOptions(); chromeOptions.addArguments("--remote-allow-origins=*");
	 * WebDriverManager.chromedriver().setup(); driver = new
	 * ChromeDriver(chromeOptions); }
	 */
	
//	public static String getScreenShot(WebDriver driver, String screenshotName) throws Exception {
//		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
//		TakesScreenshot ts = (TakesScreenshot) driver;
//		String source = ts.getScreenshotAs(OutputType.BASE64);
//		                //after execution, you could see a folder "FailedTestsScreenshots" under src folder
//		//String destination = ".\\reports\\" + screenshotName+dateName+".png"; 
//		//File finalDestination = new File(destination);
//		//FileUtils.copyFile(source, finalDestination);
//		return source;
//		}
	
	public static String getScreenShot1(WebDriver driver, String screenshotName) throws Exception {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		                //after execution, you could see a folder "FailedTestsScreenshots" under src folder
		String destination = ".\\reports\\" + screenshotName+dateName+".png"; 
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
		}
	
	//@BeforeSuite(alwaysRun=true)
	public void beforeSuite()
	{	
		report = new ExtentReports(".\\reports\\"+"ExtentReportResults.html");		
		report.addSystemInfo("Environment", "QA");
		report.addSystemInfo("Location", "Xcel Energy");
	}
	
	//@BeforeMethod(alwaysRun=true)
	public void beforeMethod()
	{	//driver = startDriver();		
//		ChromeOptions chromeOptions = new ChromeOptions();
//		//chromeOptions.addArguments("headless");
//		chromeOptions.addArguments("--remote-allow-origins=*");
//		WebDriverManager.chromedriver().setup(); 
		
		EdgeOptions edgeOptions = new EdgeOptions();
		//edgeOptions.addArguments("headless");
		edgeOptions.addArguments("--remote-allow-origins=*");
		WebDriverManager.edgedriver().setup();
		
		//driver = new ChromeDriver(chromeOptions);
		driver = new EdgeDriver(edgeOptions);
		
		driver.manage().window().maximize();
	}
	
	//@AfterMethod(alwaysRun=true)
	public void AfterMethod(ITestResult result) throws Exception	{
		//if(result.getStatus() == ITestResult.FAILURE)
		//{
			//test.log(LogStatus.PASS, "Test Case Failed is "+result.getName());
			//test.log(LogStatus.PASS, "Test Case Failed is "+result.getStatus());
			//test.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
			//To capture screenshot path and store the path of the screenshot in the string "screenshotPath"
			                        //We do pass the path captured by this mehtod in to the extent reports using "logger.addScreenCapture" method.
				//String screenshotPath = getScreenShot(driver, result.getName());
			//To add it in the extent report
				//test.log(LogStatus.PASS, test.addScreenCapture(screenshotPath));
				test.log(LogStatus.PASS, "Above line is original");
			//test.log(LogStatus.FAIL, test.addBase64ScreenShot(screenshotPath));
		//}
//		else if(result.getStatus() == ITestResult.SKIP)
//		{
		//test.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
//		}
		
		report.endTest(test);		
		driver.close();
	}
	
	//@AfterSuite(alwaysRun=true)  
	public void afterSuite() 
	{
		report=getReport();
		report.flush();
	}
	
	public ExtentReports getReport()
	{
		return report;
	}
	
	public WebDriver getDriver()
	{
		return driver;
	}
	
	public ExtentTest getTest()
	{
		return test;
	}
	
	public void setTest(String testName, String testDescription)
	{
		try {
			test = report.startTest(testName, testDescription);
		}
		catch(Exception e) {
			System.out.println("Exception is: "+e);
		}
		//test = startTest(testName, testDescription);
		//test = report.startTest(testName, testDescription);
	}
	
//	public WebDriver startDriver()
//	{
//		ChromeOptions chromeOptions = new ChromeOptions();
//		WebDriverManager.chromedriver().setup(); 
//		WebDriver driver = new ChromeDriver(chromeOptions);	
//		return driver;
//	}
	
	public ExtentTest startTest(String testName, String testDescription)
	{
		test = report.startTest(testName, testDescription);
		return test;
	}
	
	public void addStep(String status, String stepName, String stepDescription)
	{
		if(status.equalsIgnoreCase("PASS"))
		{
			test.log(LogStatus.PASS, stepName, stepDescription);			
		}
		if(status.equalsIgnoreCase("FAIL"))
		{
			test.log(LogStatus.FAIL, stepName, stepDescription);
		}
	}
}
