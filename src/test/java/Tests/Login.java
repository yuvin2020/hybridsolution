package Tests;

import java.util.Base64;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.testng.annotations.Test;
import org.testng.ITestResult;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Validation.LoginValidation;
import io.github.bonigarcia.wdm.WebDriverManager;


public class Login extends BaseTest{
		
	//public ChromeOptions chromeOptions;
	//public WebDriver driver;
	
	@Test
	public void sampleTest1() {
		System.out.println("TestNG, Mavan and Jenkins integration");
	}
	
	@Test
	public void sampleTest11() {
		System.out.println("TestNG, Mavan and Jenkins integration");
	}
	
	@Test
	public void sampleTest12() {
		System.out.println("TestNG, Mavan and Jenkins integration");
	}
	
	@Test
	public void sampleTest13() {
		System.out.println("TestNG, Mavan and Jenkins integration");
	}
	
	@Test
	public void LoginDemoChrome() {	
		try {

			ChromeOptions chromeOptions = new ChromeOptions();
			//chromeOptions.addArguments("headless");
			chromeOptions.addArguments("--remote-allow-origins=*");
			WebDriverManager.chromedriver().setup(); 
			WebDriver driver = new ChromeDriver(chromeOptions);
									
//			EdgeOptions edgeOptions = new EdgeOptions();
//			//edgeOptions.addArguments("headless");
//			edgeOptions.addArguments("--remote-allow-origins=*");
//			WebDriverManager.edgedriver().setup();
//			WebDriver driver = new EdgeDriver(edgeOptions);
			
			// Create a new instance of the ChromeDriver
			
			// Perform your test actions using the driver
			driver.get("https://amazon.in");
			System.out.println(driver.getTitle());

			// Quit the driver
			driver.quit();
		}catch(Exception e) {
			System.out.println("Exception is: "+e);
		}		
	}
	
	@Test
	public void LoginDemoEdge() {	
		try {

//			ChromeOptions chromeOptions = new ChromeOptions();
//			//chromeOptions.addArguments("headless");
//			chromeOptions.addArguments("--remote-allow-origins=*");
//			WebDriverManager.chromedriver().setup(); 
//			WebDriver driver = new ChromeDriver(chromeOptions);			
						
			EdgeOptions edgeOptions = new EdgeOptions();
			//edgeOptions.addArguments("headless");
			edgeOptions.addArguments("--remote-allow-origins=*");
			WebDriverManager.edgedriver().setup();
			WebDriver driver = new EdgeDriver(edgeOptions);
			
			// Create a new instance of the ChromeDriver
			
			// Perform your test actions using the driver
			driver.get("https://amazon.in");
			System.out.println(driver.getTitle());

			// Quit the driver
			driver.quit();
		}catch(Exception e) {
			System.out.println("Exception is: "+e);
		}		
	}	
	
	//@Test
	public void loginTest1()
	{	
		setTest("First Test", "PASS Test");			
		driver.get("https://amazon.in");
		LoginValidation valC = new LoginValidation(driver, report, test);		
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		//driver.findElement(By.xpath("//a[@id='nav-link-accountList']")).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

		if("ABC"=="ABC")
		{
			//test.log(LogStatus.PASS, "First test cases is passed");
			test.log(LogStatus.PASS, "First test 2 step", "First test 2 step details");
		}

		valC.val();
	}
	
	//@Test
	public void loginTest2() throws Throwable
	{
		//driver = getDriver();
		//report = base.getReport();
		setTest("Second Test Password Encode and Decode", "PASS Test");
		driver.get("https://amazon.in");
		String src = "10@Cognizant";
		String encodedString = Base64.getEncoder().encodeToString(src.getBytes());
		
		byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
		String decodedString = new String(decodedBytes);
		
		if("ABC"=="ABC")
		{
			test.log(LogStatus.PASS, "Second test cases is passed");	
			test.log(LogStatus.PASS, src + " | " + encodedString + " | " + decodedString);	
		}
	}
	
	//@Test
	public void loginTest3()
	{
		//driver = getDriver();
		//report = base.getReport();
		setTest("Third Test", "PASS/FAIL Test with screenshot");
		driver.get("https://amazon.in");
		if("ABC"=="ABC")
		{
			test.log(LogStatus.PASS, "Third test cases is passed");
			test.log(LogStatus.FAIL, "Third test cases is failed");
		}
	}
	
	//@Test
	public void loginTest4()
	{	
		setTest("Fourth Test", "PASS Test");			
		driver.get("https://amazon.in");
		LoginValidation valC = new LoginValidation(driver, report, test);		
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		//driver.findElement(By.xpath("//a[@id='nav-link-accountList']")).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

		if("ABC"=="ABC")
		{
			//test.log(LogStatus.PASS, "First test cases is passed");
			test.log(LogStatus.PASS, "First test 2 step", "First test 2 step details");
		}

		valC.val();
	}
	
	//@Test
	public void loginTest5()
	{	
		setTest("Fifth Test", "PASS Test");			
		driver.get("https://amazon.in");
		LoginValidation valC = new LoginValidation(driver, report, test);		
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		//driver.findElement(By.xpath("//a[@id='nav-link-accountList']")).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

		if("ABC"=="ABC")
		{
			//test.log(LogStatus.PASS, "First test cases is passed");
			test.log(LogStatus.PASS, "First test 2 step", "First test 2 step details");
		}

		valC.val();
	}
}
